
<?php
 	include "../includes/leaflet/header_leaflet.php"; 
 	include "../pages/stayintouch.php" ;
?>
<div class="container">

<section id="leafletMap" >
<div class="row">
<div class="col-8">
	<h2>Leaflet Maps</h2>
	<div id="map"></div>

	<script>
	//script to create the map
		var map = L.map('map').setView([44.6505, -63.6062], 12);  
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> | &copy Mazzy Demo'}).addTo(map);  


		//L.Control.geocoder().addTo(map);

		map.addControl( new L.Control.Search({
			url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
			jsonpParam: 'json_callback',
			propertyName: 'display_name',
			propertyLoc: ['lat','lon'],
			marker: L.marker([0,0],{radius:5}),
			collapsed: false,
			autoType: false,
			minLength: 2,
			position: 'topleft',
			hideMarkerOnCollapse: false,
			textPlaceholder: 'Search here!',
			zoom: 14
		}) );
	</script>		
</div>
<div class="col">
	<div id="controls" class="float-right text-center"><br><h5>Directions by car:</h5></div>
</div>
</div>
<br><hr><br>
<div class="row">
  <div class="col-4">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action list-group-item-dark active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home" >Instructions</a>
      <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Drawing & Geometry tools</a>
      <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Directions & Routing</a>
      <a class="list-group-item list-group-item-action list-group-item-success" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Save Work</a>
      <a class="list-group-item list-group-item-action list-group-item-warning" id="list-load-list" data-toggle="list" href="#list-load" role="tab" aria-controls="load">Load Work</a>
    </div>
  </div>
  <div class="col-8">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list"><p class="lead"><u>Leaflet Maps <b>demo</b></u> allow you to search for any registered address around the world. This map also allow you to draw, measure, calculate, and plan your routes on the world's map for your future plans or projects.</p>
      <p class="lead">This mapping software is powerful and can be enhanced by powerful plugins and solutions that are not available on this demo. If you are interested in making your own private or public map, <b>we can <u><a href="" data-toggle="modal" data-target="#myModal" class="text-info">work on it! :)</a></u></b> </p>
      </div>

      <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list"><p class="lead">
      	You can use this tool by selecting one of the six tool boxes at the left side of the map. Here's what they do:</p>
			<div class="list-group">
  			<p class="list-group-item list-group-item-action lead"><img src="../includes/leaflet/images/misc/polyline.png"> <b><u>Polyline</u>:</b> Allow you to draw a line on the map; displays the distance of the line in meters.</p>
  			<p class="list-group-item list-group-item-action lead"><img src="../includes/leaflet/images/misc/polygon.png"> <b>Polygon:</b> Allow you to draw a closed area on the map; displays the size of the drawn area in hectars.</p>
  			<p class="list-group-item list-group-item-action lead"><img src="../includes/leaflet/images/misc/rectangle.png"> <b>Rectangle:</b> Allow you to draw a closed rectangle on the map; displays the size of the drawn rectangle in hectars.</p>
  			<p class="list-group-item list-group-item-action lead"><img src="../includes/leaflet/images/misc/circle.png"> <b>Circle:</b> Hold the mouse and drag to draw a closed circle on the map; displays the coordinates of the centre of the circle.</p>
  			<p class="list-group-item list-group-item-action lead"><img src="../includes/leaflet/images/misc/marker.png"> <b>Marker:</b> Allow you to drop a pin on the map; displays the coordinates of the pin location.</p>
  			<p class="list-group-item list-group-item-action lead"><img src="../includes/leaflet/images/misc/circlemarker.png"> <b>Circle Marker:</b> Allows you to drop a circle marker on the map; displays the coordinates of the circle marker location.</p>
  			<p class="list-group-item list-group-item-info lead"><img src="../includes/leaflet/images/misc/edit.png"> <b>Edit:</b> Allows you to move and change the location of any of the drawing tools; displays draggable white boxes.</p>
  			<p class="list-group-item list-group-item-warning lead"><img src="../includes/leaflet/images/misc/delete.png"> <b>Delete:</b> Allows you to delete one or more of the selected drawing tools. Take caution as deletes are irreversible</p>
	  		</div>
       </div>

      <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list"><p class="lead">If you <b>double click the map</b>, a small box with 'Start' and 'End' locations will appear.</p>
      	<p class="lead">At the '<a href="#controls">Directions by car</a>', you should be able to see how to get to your location by car no matter where you are on the globe.</p>
      	<p class="lead">You can add stops or reverse directions by clicking on the map or from the directions box anytime you want!</p>
      </div>
      <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list"><p class="lead">You can save your current map to finish working on it later exactly like you left it. You can Download your JSON file here; which are later required in order to load your work on this website.</p>
      	<button type="button" class="btn btn-outline-success" value='Save Work' onclick='collectData();'>Save Work</button>
      </div>
      <div class="tab-pane fade" id="list-load" role="tabpanel" aria-labelledby="list-settings-list"><p class="lead">Make every session worth a while, and come back anytime!</p>
      	<p class="lead">You can load your work back to this map anytime you want! Just do not forget to save your work before you leave! ;) </p>
      	<br>
      	<form id="jsonFile" name="jsonFile" enctype="multipart/form-data" method="post">
			<fieldset>
			<input type='file' id='fileinput'><br><hr>
			<button type="button" class="btn btn-outline-warning" value='Load Work' onclick='loadFile();'>Load Work</button>
  			</fieldset>
		</form>  
      </div>
    </div>
  </div>
</div>
<br><hr>
</section>



</div>

<?php
  include "../includes/leaflet/footer_leaflet.php"; 

?>


