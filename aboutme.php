<?php include "includes/header.php"; ?>

<section id="aboutme" class="bg-light">
<div class="container">
<div class="row">
	<div class="col-3-md">
	<img src="img/me_nates.jpg" class="img-thumbnail" title="This is me! Picture is courtesy of Nathaniel Cole" style="border-radius: 8px; margin-right: 30px; margin-bottom: 15px; float: left;">
	</div>
	
	<div class="col-md mx-auto text-justify">
	<h2>About me.</h2>
	<p class="lead">My name is <b>Emad Bamatraf </b>a student Dalhousie University who enjoys web design and coding.</p> 
	<p class="lead">I'm starting this website as an online portfolio and to write about interesting stuff. Let's connect on MazzyMoo and have fun! &#128513;</p>
	<hr>
	<p class="lead">We can work to build nice websites for you! Check my portfolio &#128521;</p>

<!-- Trigger the modal with a button -->
<?php include "pages/stayintouch.php" ;?>
	<p class="lead">Whether you have a project you want to start, or work in-progress let us <b><a href="" data-toggle="modal" data-target="#myModal"> get in touch!</a></b></p>
</div>
</div>


</div>
</section>

<?php include "includes/footer.php"; ?>