  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content"> 
<div class="modal-header">
  <h4 class="modal-title text-primary">Let us stay in touch!</h4>
  <button type="button" class="close" data-dismiss="modal">&times;</button> 
</div>
<div class="modal-body mx-auto">
<form method="post" action="<?php
if($currentFileName=='index.php') {echo "index.php";}
else {echo "../index.php";}
?>">
  <div class="form-group">
    <div class="form-group">
      <input id="fname" name="fname" type="text" class="form-control form-control-lg" placeholder="Full Name*" required>
    </div>
  </div>
  <div class="form-group">
    <input id="email" name="email" type="Email" class="form-control form-control-lg" placeholder="Email*" required="">
  </div>
    <div class="form-group">
    <input id="subject" name="subject" type="text" class="form-control form-control-lg" placeholder="Subject">
    </div>
  
  <div class="form-group">
    <label for="exampleFormControlTextarea1" class="lead">Comments:</label>
    <textarea class="form-control form-control-lg" id="comments" name="comments" rows="3" required="" placeholder="Your comments*"></textarea>
  </div>
  <br>
  <button id="submitContact" type="submit" name="submitContact" class="btn-lg btn-success btn-block">Submit</button>
</form>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
      
    </div>
  </div>