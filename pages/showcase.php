<div class="col-md mx-auto">

<h2>Recent Projects.</h2>
<p class="lead">Here is information about projects that I worked on.</p>
<br>


<div class="row ">
	<div class="col text-center hover-blur">
	<a href="https://ecologyaction.ca/walking-school-bus" title="" target="_blank">
	    <img src="img/twb.png" class="img-thumbnail img-fluid">
	    <h2><span class="small text-white">Leaflet</span></h2>
	</a>
	<p><b>The Walking Bus </b><br><div class="small"> Private Mapping Application</div></p>
	</div>
	
	<div class="col text-center hover-blur">
	<a href="https://www.mircs.ca/aboutus/interns/" title="" target="_blank">
	    <img src="img/mircs.png" class="img-thumbnail img-fluid">
	    <h2><span class="small text-white">Laboratory Prototype</span></h2>
	</a>
	<p><b>MIRCS Institute</b> <br><div class="small"> Prototype Navigation & Optimization</div></p>
	</div>
	
	<div class="col text-center hover-blur">
	<a title="" target="_blank" href="http://ethicsinpractice.ca/contributors/">
	    <img src="img/bioethics.png" class="img-thumbnail img-fluid" width="200" height="200">
	    <h2><span class="small text-white">Consultation Tool</span></h2>
	</a>
	<p class="text-dark"><b>Ethics In Practice </b><div class="small">A Bioethics Consultation Tool </div> </p>
	</div>
	
</div>

<hr>
	<h2>Personal Code Demo(s).</h2>
	<p class="lead">Here are demos of some code.</p>
	<br>

<div class="row ">

	<div class="col-4 text-center hover-blur mx-auto">
		<div class="col text-center hover-blur">
		<a href="projects/maps" title="Something like Google Maps!">
		    <img src="img/gis.png" class="img-thumbnail img-fluid">
		    <h2><span class="small text-white">Try it out!</span></h2>
		</a>
		<p>Private Trip Planner</p>
		</div>
	</div>


</div>	


