<?php 
	include "includes/header.php";
?>


<section id="contactMe" class="bg-light">
<div class="container">
<div class="row">
<?php 
if(isset($_GET['commentError'])) {
?>
	<div class="col-md-5 mx-auto">
	<div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
	  <strong>Whoops, </strong> this doesn't usually happen. <br>Please reach me though: <br> <a href="mailto:emazzy@mazzymoo.com">emazzy@mazzymoo.com</a> 
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>	
	</div>
<?php
}
if(isset($_GET['commentReported'])) {
?>
	<div class="col-md-5 mx-auto">
	<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
		<strong>Sweet! </strong> <br> We got your message and I'll get back to you as soon as possible =) 
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
	</div>	
	</div>
<?php
}
?>
	</div>
<div class="row">
<div class="col-lg-8 mx-auto">
<br>
	<h2>Contact me.</h2>
	<p class="lead">We can work on your next project together. Or for further questions or enquires, please contact me :)</p>

<hr><br>

<form method="post" action="contactme.php">
	<div class="form-group">
		<div class="form-group">
			<input id="ufname" name="ufname" type="text" class="form-control form-control-lg" placeholder="Full Name*" required>
		</div>
	</div>
	<div class="form-group">
		<input id="uemail" name="uemail" type="Email" class="form-control form-control-lg" placeholder="Email*" required="">
	</div>
	<div class="form-group">
		<label for="exampleFormControlTextarea1" class="lead">Subject:</label>
		<select class="form-control" id="typeOfIssue" name="typeOfIssue">
			<option></option>
			<option value="greet">Say Hi!</option>
			<option value="enquiry">Feedback or Enquiry</option>
		</select>
	</div>
	<div class="form-group">
		<label for="exampleFormControlTextarea1" class="lead">Comments:</label>
		<textarea class="form-control form-control-lg" id="ucomments" name="ucomments" rows="3" placeholder="Your comments*" required=""></textarea>
	</div>
	<br>
	<button id="submitComment" type="submit" name="submitComment" class="btn-lg btn-success btn-block">Submit</button>
</form>



</div>
</div>
</div>

</section>



<?php
	include "includes/footer.php";
?>