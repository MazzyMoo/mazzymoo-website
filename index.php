<?php
 include "includes/header.php"; 

 if(isset($_GET['commentReported'])) {
?>
<div class="bg-block text-center">
<div class="alert alert-info alert-dismissible fade show" role="alert">
  <strong>Thank you!</strong> Your comment has been sent successfully!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>	
</div>

<?php }
?>
<div class="attraction bg-custom text-light"  id="home">
	<div class="vid_container">
	<video autoplay muted loop class="backgroundvd">
  		<source src="img/background.mp4" type="video/mp4" >
	</video>
	</div>

	<div class="attraction_info">
	<div class="col">
	  <h1 id="m_title">MazzyMoo</h1>
	  <h2 id="m_subtitle">A World Within Brackets</h2>
	 </div>
	</div>

<?php 
	
if($_GET['subalert']=='false') {
	?>
	<div class="col-md-5 mx-auto">
	<br>
	<div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
	  <strong>You're already registered!</strong> Thanks for coming again!
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>	
	</div>
<?php
	}

else if($_GET['subalert']=='true') {
?>
<div class="col-md-5 mx-auto">
<br>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Thank you</strong> for signing up!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>	
</div>

<?php 
} 
else if($_GET['subalert']=='error') {
?>
<div class="col-md-5 mx-auto">
<br>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>Error!</strong> Whoops.. something went wrong, please try again later!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>	
</div>
<?php } ?>
</div>
<?php if(!empty($_GET['getintouch'])=='success') { ?>
<div class="col-md-5 mx-auto">
<br>
<div class="alert alert-info alert-dismissible fade show" role="alert">
  <strong>Thank you</strong> Your message is sent. You shoud expect a reply soon!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>	
</div>
<?php } 
else if(!empty($_GET['getintouch'])=='error'){ ?>
	<div class="col-md-5 mx-auto">
	<br>
	<div class="alert alert-warning alert-dismissible fade show" role="alert">
	  <strong>Sorry!</strong> There is an error. Report this if you think it is a bug!
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>	
	</div>
<?php } ?>


<section id="ourFeed">
<div class="container text-center">
	<h2>Our Feed.</h2>
	<p class="lead tex">Join our discussion</p>
	<br>
	<div class="row text-center">
		<div class="col-md" style="padding: 15px 15px 15px 15px;">
			<div class="card mx-auto" style="width: 320px;" >
			  <a href="feed/articles"><img src="img/thefeed.png" class="card-img-top " alt="Our Feed" ></a>
			  <div class="card-body">
			    <p class="card-text lead">It is almost 2020. Posting articles is my New Year's resolution, what is yours?<hr><a href="feed/articles">Join the blog, let us know!</a></p>
			  </div>
			</div>
		</div>
	</div>
<script type="text/javascript">
setInterval(function(){
    $('.card').toggleClass('magictime boingInUp');
}, 2000 );
</script>
<?php
if(isset($_GET['subalert'])!='true') {
?>
<hr>

<form method="post" action="index.php">
  <div class="row mx-auto" style="max-width: 60%;">
    <div class="col">
      <input id="newssignup" name="newssignup" type="email" class="form-control form-control-lg mb-1" placeholder="Sign up now!">
    </div>
    <div class="col-md-4 text-center">
	<button id="subscribebutton" type="submit" name="subscribebutton" class="btn-lg btn-success " >Submit</button>    </div>
  </div>
</form>



</div>
<?php
}
?>
<p class="blockquote-footer text-dark text-center"><b>Sign up with your email to recieve updates!</b></p>
</div>
</section>


<section id="showcase" class="bg-light">
<div class="container">
<div class="row">
	
	
	<?php include "pages/showcase.php"; ?>

</div>
</div>
</section>


<?php include "includes/footer.php"; ?>


