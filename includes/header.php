<?php
  
  include_once "includes/db.php"; 
  include_once "includes/functions.php"; 
	//cookie settings

if(isset($_POST['submitContact'])) {
  $fname = sanitize($_POST['fname']);
  $email = sanitize($_POST['email']);
  $subject = sanitize($_POST['subject']);
  $comments = sanitize($_POST['comments']);

  $sql = "INSERT INTO getintouch(Name, Email, Subject, Comment, Time) VALUES ('$fname', '$email', '$subject', '$comments', '$currDate')";
  $result_sql = mysqli_query($conn, $sql);

  if($result_sql) {
  $userMessage = <<<_ENDISSUEREPORT
Get In Touch [Message]

Submitted by: {$fname}
Email ID: {$email}

Subject: {$subject}
Comments: {$comments}

_ENDISSUEREPORT;

  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

  $sendmail = mail("emazzy@mazzymoo.com","Get_inTouch", $userMessage, $headers);

  $issueDetail['fileName'] = "misc/inTouch_" . $issueDetail['timeStamp'] . ".txt";
  $issueFileHandle = fopen($issueDetail['fileName'], "w") or die("Sorry! Unable to open file!");

  fwrite($issueFileHandle, $userMessage);

  fclose($issueFileHandle);

  if($sendmail == true)
    header("Location: index.php?getintouch=success");
  //add confirmation
  }

  else {
    //echo "error!!".mysqli_connect_error();
    header("Location: index.php?getintouch=error");
  }
}
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="description" content="Web Development & Community">
  <meta name="keywords" content="Portfolio,Emad,Bamatraf,MazzyMoo,MazMozdy,, MazMozdy, Maz, Mazzy, Mozdy, Moo, Halifax, NS, Canada, Aden, Yemen,MIRCS,BioEthics,the walking bus, personal website,thefeed,blog,vlog,a world within brackets,desgin,projects,halifax,canada,novascotia,dalhousie,bacs,computerscience,international,leaflet">
  <meta name="author" content="Emad Bamatraf, MazzyMoo">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Mazzy Personal Website</title>

  <!-- CSS files -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Viga&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Merriweather:700&display=swap" rel="stylesheet">

  <link href="styles/style.css" rel="stylesheet">
  <link href="/styles/magic-master/dist/magic.css" rel="stylesheet" type="text/css">

</head>

<nav class="navbar navbar-expand-sm navbar-light sticky-top" id="mainNav" style="">
<div class="container">
<a class="navbar-brand js-scroll-trigger" href="/"><img src="img/brandbox.png" width="30" height="30" class="d-inline-block align-top" href="#brand"> Mazzy</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav ml-auto">
  <li class="nav-item dropdown ">
    <a class="nav-link js-scroll-trigger" href="/#ourFeed">Our Feed</a>
  </li>
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="/#showcase">Showcase</a>
  </li>
</ul>
</div>
</div>
</nav>


<body class="page-top">
