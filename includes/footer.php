<?php 
if(isset($_POST['logoutbutton'])) {
	session_destroy();
	header("Location: ".$currentFileName);
}
?>



<footer class="footer py-5 bg-dark text-light " id="mainFooter">
    <div class="container lead">
      <p class="m-0 text-right small">Copyright &copy;  Emad Bamatraf <?php echo date('Y'); ?></p>
<hr>
<div class="row">
<div class="col-auto mr-auto">
  <ul class="list-group ">
	<li class="list-group"><a href="/contactme" class="text-light">Contact me</a></li>
	<li class="list-group"><a href="/aboutme" class="text-light">About me</a></li>
	<li class="list-group"></li>
  </ul>
</div>
<div class="col-auto">
  <a href="https://twitter.com/Mazzy18291625" target="_blank"><img src="/img/twitter.png"></a>
  <a href="https://www.linkedin.com/in/emad-bamatraf-532977190" target="_blank"><img src="/img/Linkdin.png"></a>
</div>
</div>


    </div>
</footer>

	<script src="/styles/vendor/jquery/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
	<script src="/styles/scrolling-nav.js"></script>
	  <!-- Plugin JavaScript -->
	<script src="/styles/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="/styles/mdb.min.js"></script>

</body>
</html>
