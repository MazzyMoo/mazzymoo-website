<?php
	$db_host = "your_host";
	$db_username = "your_username";
	$db_password = "your_password";
	$db_name = "your_database";

	$conn = mysqli_connect($db_host, $db_username, $db_password, $db_name);
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}

	include_once "includes/initPubDB.php";
?>