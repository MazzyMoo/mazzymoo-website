<?php
$currDate = date("Y-m-d H:i:s");

$currentFileName = basename($_SERVER['PHP_SELF']);
$dateTimeObject = new DateTime("now", new DateTimeZone("America/Halifax"));
$dateTimeObject->setTimestamp(time()); //adjust the object to correct timestamp
$issueDetail['issueDate'] = $dateTimeObject->format('d.m.Y');
$issueDetail['issueTime'] = $dateTimeObject->format('h:i:sa');
$issueDetail['timeStamp'] = time();

function sanitize ($sanitizeThisThing) {
	$sanitizedThing = trim($sanitizeThisThing);
	$sanitizedThing = stripslashes($sanitizedThing);
	$sanitizedThing = htmlspecialchars($sanitizedThing);

	return $sanitizedThing;
}

if(isset($_POST['subscribebutton'])) {
	$subemail = sanitize($_POST['newssignup']);
	
	$sql1 = "SELECT email FROM subscribers WHERE email='$subemail'";
	$result_sql1 = mysqli_query($conn, $sql1);
	
	if (mysqli_num_rows($result_sql1)) {
	    echo "Error: Duplicate found!";
	    header("Location: ".$currentFileName."?subalert=false");
	}
	else 
	{
		$sql = "INSERT INTO subscribers(email, date) VALUES('$subemail', '$currDate')";
		if (mysqli_query($conn, $sql)) {
		    echo "New record created successfully";
		  	header("Location: ".$currentFileName."?subalert=true");
		}
		else {
		    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		    header("Location: ".$currentFileName."?subalert=error");
		}
	}
}//end of subscribe button function



//contactme form
if(isset($_POST['submitComment'])) {
  $issueDetail['submittedfname'] = sanitize($_POST['ufname']);
  $issueDetail['submittedemail'] = sanitize($_POST['uemail']);
  $issueDetail['submittedissue'] = sanitize($_POST['typeOfIssue']);
  $issueDetail['submittedcomment'] = sanitize($_POST['ucomments']);


switch ($issueDetail['submittedissue']) {
  case "greet":
    $issueDetail['submittedissue'] = "Say Hi!";
    break;
  case "enquiry":
    $issueDetail['submittedissue'] = "Feedback or Enquiry";
    break;
  default:
    $issueDetail['submittedissue'] = "Not Selected";
}

$issueDetail['detailedReport'] = sanitize($_POST['detailedReport']);

  $issueMessage = <<<_ENDISSUEREPORT
HomePage Comment
Type of comment: {$issueDetail['submittedissue']}

Submitted by: {$issueDetail['submittedfname']} 
Email ID: {$issueDetail['submittedemail']}
Submitted at: {$issueDetail['issueTime']} - on - {$issueDetail['issueDate']}

Details: {$issueDetail['submittedcomment']}
_ENDISSUEREPORT;

  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

  $sendmail = mail("emazzy@mazzymoo.com","Website_Comment", $issueMessage, $headers);

  $issueDetail['fileName'] = "misc/message_" . $issueDetail['timeStamp'] . ".txt";
  $issueFileHandle = fopen($issueDetail['fileName'], "w") or die("Sorry! Unable to open file!");

  fwrite($issueFileHandle, $issueMessage);

  fclose($issueFileHandle);

  if($sendmail == true)
   header("Location: " . $currentFileName . "?commentReported");
	else 
		header("Location: " . $currentFileName . "?commentError");
}


?>

