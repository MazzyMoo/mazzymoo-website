<?php 
include "../../includes/db.php"; 
include "../../includes/functions.php"; 
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Mazzy Personal Website</title>

  <!-- CSS files -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">



<link href="../styles/style.css" rel="stylesheet">

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
crossorigin=""/>
<link rel="stylesheet" href="../includes/leaflet/Leaflet.draw-develop/src/leaflet.draw.css" />
<link rel="stylesheet" href="../includes/leaflet/leaflet-search.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.css" />

<!-- Leaflet files -->
<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
crossorigin=""></script>
<script type="text/javascript" src="../includes/leaflet/leaflet-search.js"></script>
<script src="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.js"></script>
<script src="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.js"></script>

<!-- draw --> 
<script src="../includes/leaflet/Leaflet.draw-develop/src/Leaflet.draw.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/Leaflet.Draw.Event.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/Toolbar.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/Tooltip.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/ext/GeometryUtil.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/ext/LatLngUtil.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/ext/LineUtil.Intersect.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/ext/Polygon.Intersect.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/ext/Polyline.Intersect.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/ext/TouchEvents.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/draw/DrawToolbar.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/draw/handler/Draw.Feature.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/draw/handler/Draw.SimpleShape.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/draw/handler/Draw.Polyline.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/draw/handler/Draw.Marker.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/draw/handler/Draw.CircleMarker.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/draw/handler/Draw.Circle.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/draw/handler/Draw.Polygon.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/draw/handler/Draw.Rectangle.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/edit/EditToolbar.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/edit/handler/EditToolbar.Edit.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/edit/handler/EditToolbar.Delete.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/Control.Draw.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/edit/handler/Edit.Poly.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/edit/handler/Edit.SimpleShape.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/edit/handler/Edit.Marker.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/edit/handler/Edit.CircleMarker.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/edit/handler/Edit.Circle.js"></script>
<script src="../includes/leaflet/Leaflet.draw-develop/src/edit/handler/Edit.Rectangle.js"></script>   




<nav class="navbar navbar-expand-sm navbar-light sticky-top" id="mainNav" style="">
<div class="container">
<a class="navbar-brand js-scroll-trigger" href="/"><img src="../img/brandbox.png" width="30" height="30" class="d-inline-block align-top" href="#brand"> Mazzy</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav ml-auto">
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="/">Home</a>
  </li>
  <li class="nav-item dropdown ">
    <a class="nav-link js-scroll-trigger" href="/#ourFeed">Our Feed</a>
  </li>
  <li class="nav-item dropdown ">
    <a class="nav-link js-scroll-trigger" href="./#showcase">Showcase</a>
  </li>
</ul>
</div>
</div>
</nav>



<body class="page-top">


