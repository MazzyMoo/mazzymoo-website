function collectData() {
    var collection = {
            "type": "FeatureCollection",
            "features": []
        };

    // Iterate the layers of the map
    map.eachLayer(function (layer) {
        // Check if layer is a marker
        if (layer instanceof L.Marker) {
            // Create GeoJSON object from marker
            var geojson = layer.toGeoJSON();
            // Push GeoJSON object to collection
            collection.features.push(geojson);
        }        
    });
    downloadLeafletDraw(JSON.stringify(drawnItems.toGeoJSON(), null, 2));
}

function downloadLeafletDraw(jsonData) {
    let dataStr = jsonData;
    let dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(dataStr);
    
    let exportFileDefaultName = 'routes.json';
    
    let linkElement = document.createElement('a');
    linkElement.setAttribute('href', dataUri);
    linkElement.setAttribute('download', exportFileDefaultName);
    linkElement.click();
}