



<footer class="footer py-5 bg-dark text-light " id="mainFooter">
    <div class="container lead">
      <p class="m-0 text-center text-black">Copyright &copy; Emad Bamatraf 2019</p>
<hr>
      <ul class="list-group">
		<li class="list-group"><a href="/contactme">Contact me</a></li>
		<li class="list-group"><a href="/aboutme">About me</a></li>
		<li class="list-group"></li>
	  </ul>
    </div>
</footer>

<script src="../styles/vendor/jquery/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="../styles/scrolling-nav.js"></script>
  <!-- Plugin JavaScript -->
<script type="text/javascript" src="../includes/leaflet/leafletdraw.js"></script>
<script type="text/javascript" src="../includes/leaflet/export.js"></script>
<script type="text/javascript" src="../includes/leaflet/load.js"></script>
<script type="text/javascript" src="../includes/leaflet/leaflet-search.js"></script>
<script type="text/javascript" src="../includes/leaflet/routing.js"></script>


</body>
</html>





