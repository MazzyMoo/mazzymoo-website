var control = L.Routing.control({
    waypoints: [],
    geocoder: L.Control.Geocoder.nominatim(),
    routeWhileDragging: true,
    reverseWaypoints: true,
    fitSelectedRoutes: true,

    waypointNameFallback: function(latLng) {
    function zeroPad(n) {
        n = Math.round(n);
        return n < 10 ? '0' + n : n;
    }
    function sexagesimal(p, pos, neg) {
    var n = Math.abs(p),
        degs = Math.floor(n),
        mins = (n - degs) * 60,
        secs = (mins - Math.floor(mins)) * 60,
        frac = Math.round((secs - Math.floor(secs)) * 100);
        return (n >= 0 ? pos : neg) + degs + '°' +
        zeroPad(mins) + '\'' +
        zeroPad(secs) + '.' + zeroPad(frac) + '"';
    }
    return sexagesimal(latLng.lat, 'N', 'S') + ' ' + sexagesimal(latLng.lng, 'E', 'W');
    }
});

var routeBlock = control.onAdd(map);    
document.getElementById('controls').appendChild(routeBlock);


function createButton(label, container) {
	var btn = L.DomUtil.create('button', '', container);
	btn.setAttribute('type', 'button');
	btn.innerHTML = label;
    return btn;
}

map.on('dblclick', function(e) {
    var container = L.DomUtil.create('div'),
        startBtn = createButton('Start from this location', container),
        destBtn = createButton('Go to this location', container);
    L.popup()
        .setContent(container)
        .setLatLng(e.latlng)
        .openOn(map);
  	L.DomEvent.on(startBtn, 'click', function() {
        control.spliceWaypoints(0, 1, e.latlng);
        map.closePopup();
    });
    L.DomEvent.on(destBtn, 'click', function() {
        control.spliceWaypoints(control.getWaypoints().length - 1, 1, e.latlng);
        map.closePopup();
        map.off('click');
    });
});

