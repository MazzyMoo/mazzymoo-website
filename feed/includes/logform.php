<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Login</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	</div>
<form method="post" action="">
	<div class="modal-body">
		<div class="form-group">
		    <label for="log_email">Username</label>
		    <input type="text" class="form-control" id="log_email" name="log_email" aria-describedby="emailHelp" placeholder="Email" required="">
		    <small id="tip" class="form-text text-muted">Your information is encrypted and we'll never share your information with anyone else.</small>
		</div>
		  <div class="form-group">
		    <label for="log_pwd">Password</label>
		    <input type="password" class="form-control" id="log_pwd" name="log_pwd" placeholder="Password" required="">
		  </div>
		<small id="tip" class="form-text text-danger text-left">New user? <a href="register.php">Register now!</a></small>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button id="loginbutton" name="loginbutton" type="submit" class="btn btn-primary">Submit</button>
	</div>
</form>

</div>
</div>
</div>