<?php

if(!mysqli_query($conn, "DESCRIBE 'login'")) {
		$sql4 = "CREATE TABLE login (
		id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
		email VARCHAR(20) NOT NULL, 
		password VARCHAR(60) NOT NULL,  
		reg_date DATETIME)";
		if(mysqli_query($conn, $sql4))
			echo "mysql success: login";
	}

	if(!mysqli_query($conn, "DESCRIBE 'user'")) {
		$sql2 = "CREATE TABLE user (
		id INT(6) UNSIGNED, FOREIGN KEY (id) REFERENCES login(id),
		role VARCHAR(30) NOT NULL DEFAULT 'user',
		username VARCHAR(30) NOT NULL, 
		name VARCHAR(30) NOT NULL, 
		picture TEXT(30000), 
		description TEXT(1000), 
		reg_date DATETIME)";

		if(mysqli_query($conn, $sql2))
			echo "mysql success: User";
	}


	if(!mysqli_query($conn, "DESCRIBE 'category'")) {
		$sql3 = "CREATE TABLE category (
		id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
		name VARCHAR(20) NOT NULL, 
		total_posts INT(5) NOT NULL DEFAULT '0', 
		reg_date DATETIME)";
		if(mysqli_query($conn, $sql3)) {
			echo "mysql success: category";
		
		
		$sql3_inst = "INSERT INTO category(name) VALUES ('Software Dev'), ('Web Development'),('Life'),('Travel'),('Cooking')";
		if(mysqli_query($conn, $sql3_inst))
			echo "mysql success: categories installed successfully";
		}
	}

	if(!mysqli_query($conn, "DESCRIBE 'post'")) {
		$sql1 = "CREATE TABLE post (
		id INT(4) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL, 
		user_id int(6) UNSIGNED NOT NULL, FOREIGN KEY (user_id) REFERENCES user(id),
		category_id INT(3) UNSIGNED, FOREIGN KEY (category_id) REFERENCES category(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
		title VARCHAR(30) NOT NULL, 
		content TEXT(3000) NOT NULL,  
		thumbnail VARCHAR(255), 
		status VARCHAR(10) NOT NULL, 
		views int(11) UNSIGNED NOT NULL DEFAULT '0', 
		post_date DATETIME NOT NULL) "; //change it from timestamp to localTime
		$result = mysqli_query($conn, $sql1);
		if($result)
			echo "mysql success: post";
	}
	
	if(!mysqli_query($conn, "DESCRIBE 'comment'")) {
		$sql5 = "CREATE TABLE `comment` ( 
			`id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL ,
			`post_id` INT(6) UNSIGNED NOT NULL , 
			`username` VARCHAR(30) NOT NULL , 
			`description` VARCHAR(500) NOT NULL , 
			`date` DATETIME NOT NULL, 
			FOREIGN KEY (post_id) REFERENCES post(id) ON DELETE NO ACTION ON UPDATE NO ACTION ) ";
		$result = mysqli_query($conn, $sql5);
		if($result)
			echo "mysql success: comment";
	} 



?>

