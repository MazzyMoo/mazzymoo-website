<?php

session_start();
include_once $_SERVER['DOCUMENT_ROOT']."/feed/includes/db.php"; 
include_once $_SERVER['DOCUMENT_ROOT']."/feed/includes/functions.php"; 
?>
 
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="keywords" content="Emad,Bamatraf,MazzyMoo,MazMozdy,articles, cooking, programming, Emad Bamatraf, Maz, Mazzy, Mozdy, Moo, Halifax, NS, Canada, Aden, Yemen, website,thefeed,blog,vlog,a world within brackets,projects,halifax,canada,novascotia,dalhousie,bacs,computerscience,international">

  <title>Mazzy: Our Feed</title>

  <!-- CSS files -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">

  <link href="/styles/style.css" rel="stylesheet">
  <link href="./styles/base.css" rel="stylesheet" type="text/css">

</head>

<nav class="navbar navbar-expand-sm navbar-light sticky-top" id="mainNav">
<div class="container">
<a class="navbar-brand js-scroll-trigger" href="/"><img src="/img/brandbox.png" width="40" height="40" class="d-inline-block align-top" alt=""> Mazzy</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <a class="small text-warning">Feed</a>
<ul class="navbar-nav ml-auto">
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger <?php if($currentFileName=='articles.php') {echo 'show';} ?>" href="/feed/articles">Articles</a>
  </li>
  <?php if(!empty($_SESSION['logged'])==false) { ?>
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="" data-toggle="modal" data-target="#exampleModal">Login</a>
  </li>
  <?php } ?>

  <?php if(!empty($_SESSION['logged'])==true) { ?>
  <li class="nav-item">
    <form id="login_modal" method="post" action="/feed/articles">
      <button id="logoutbutton" name="logoutbutton" type="submit" class="btn btn-link nav-link" style="font-size: inherit;" >Logout</button>
    </form>
  </li>
  <?php } ?>

</ul>
</div>
</div>
</nav>

<body>
<?php include_once $_SERVER['DOCUMENT_ROOT']."/feed/includes/logform.php";

if(!empty($_SESSION['logged']) && ($_SESSION['role'])=='super') {
?>
<ul class="nav justify-content-end">
  <li class="nav-item">
    <div class="lead small text-right" style="margin-top: 5px; margin-right: 15px;"><a href="/feed/admin/dashboard">Dashboard</a></div>
  </li>
  <li class="nav-item">
  <div class="lead small text-right" style="margin-top: 5px; margin-right: 15px;"><a href="/feed/admin/clientsupport">Client Support</a></div>
  </li>
</ul>
<?php } 
if(empty($_SESSION['logged']) && $currentFileName!='register'){ ?>  
  <p class="lead small text-right" style="margin-top: 5px; margin-right: 15px;">Not part of the crew? You can <a href="register">Reigster Now!</a></p>
<?php } 
if($currentFileName=='ourfeed' && $_SESSION['logged']){ ?>  
  <div class="alert alert-success text-center" role="alert" >
    Welcome back <b><?php echo $_SESSION['username']?></b>. Enjoy your day ;)
  </div>
<?php
}
if(isset($_SESSION['logged']) && isset($_SESSION['role'])=='user' || isset($_SESSION['role'])=='super') {
?>
<ul class="nav justify-content-end">
  <li class="nav-item">
    <div class="lead small text-right" style="margin-top: 5px; margin-right: 15px;"><a href="/feed/userdash">Dashboard</a></div>
  </li>
</ul>
<?php } ?>

