<?php
$currDate = sanitize(date("Y-m-d H:i:s"));

$currentFileName = basename($_SERVER['PHP_SELF']);
$dateTimeObject = new DateTime("now", new DateTimeZone("America/Halifax"));
$dateTimeObject->setTimestamp(time()); //adjust the object to correct timestamp
$issueDetail['issueDate'] = $dateTimeObject->format('d.m.Y');
$issueDetail['issueTime'] = $dateTimeObject->format('h:i:sa');
$issueDetail['timeStamp'] = time();

function sanitize ($sanitizeThisThing) {
	$sanitizedThing = trim($sanitizeThisThing);
	$sanitizedThing = stripslashes($sanitizedThing);
	$sanitizedThing = htmlspecialchars($sanitizedThing);

	return $sanitizedThing;
}

//the old image upload replacement
function imgUpload($filename) {
	 $filename = $_FILES["personalthumb"]["name"]; //path will probably need diagnostics -> echo $target_file;
	 $filetype = $_FILES['personalthumb']['type']; 
	 $filename = strtolower($filename);
	 $filetype = strtolower($filetype);	
	 $file_ext = strrchr($filename, '.');
	 
	//check if file contain php and kill it 
	if(!((strpos($filename,'php')) === false)) {
		die('error; php detected.');
	}
	if($file_ext=="") { //if no img is uploaded or user is using default img.
		//for registration?
		//insert default img here.
		//$filename = "default.png";
		//$file_ext = ".png";
		//$thumbnail_name = "default";
	}
	else{
		$file_ext = strrchr($filename, '.');
		//check if the extension is allowed or not
		$whitelist = array(".jpg",".jpeg",".gif",".png"); 
		if(!(in_array($file_ext, $whitelist))) {
			die('<b>'.$file_ext.'</b> Is NOT an allowed extension.<em> Please upload images only!</em>'.$filename);
		}
		$pos = strpos($filetype,'image');
		//check upload type
		if($pos === false) {
			die('<b>Error: </b>File type is not image.');
		}
		$imageinfo = getimagesize($_FILES['personalthumb']['tmp_name']);
		if($imageinfo['mime'] != 'image/gif' && $imageinfo['mime'] != 'image/jpeg'&& $imageinfo['mime'] != 'image/jpg'&& $imageinfo['mime'] != 'image/png') {
			die('<b>Error: </b> Image size is a no no. Go back and try again =)'. $imageinfo['mime']);
		}
		//check double file type (image with comment)
		if(substr_count($filetype, '/')>1){
			die('<b>Error: </b>Bad file type.');
		}
		$thumbnail_name = md5(basename($_FILES['personalthumb']['name'])); //hash it boo
	}

		//uploading: 
		$uploaddir = "../img/users/";
		if (file_exists($uploaddir)) {} 
		else {  
			mkdir($uploaddir, 0777);  
		}  
		//change the image name
		$uploadfile = $uploaddir . $thumbnail_name .$file_ext;

		$usr_pic = $thumbnail_name.$file_ext; //sanitize here. --> IMPORTANTO

		if ((move_uploaded_file($_FILES['personalthumb']['tmp_name'], $uploadfile))) 
			echo "naisu.";
		else 
			echo "<br>error: file did not upload, either because you didn't provide an image; otherwise, report it please.";

		return $usr_pic; //returns the filename as a String.
}


if(isset($_POST['logoutbutton'])) { //logout functionality
	session_destroy();
	if($currentFileName=="showpost.php"){
		session_destroy();	
		header("Location: ".$_GET['article']);
	}
	else {
		session_destroy();	
		header("Location: articles");
	}

}

if(isset($_POST['loginbutton'])) { //login functionality in modal
	$emailfield = sanitize($_POST['log_email']);
	$passwordfield = sanitize($_POST['log_pwd']);

	$sql = "SELECT * FROM login WHERE email='$emailfield'";
	$result = mysqli_query($conn, $sql);
	if(mysqli_num_rows($result) == 1) {	
		if($row = mysqli_fetch_assoc($result)) { 
			$hash = $row['password'];
			$id = $row['id'];
		if(crypt($passwordfield, $hash) == $hash){
			$s1 = "SELECT * FROM user WHERE id='$id'";
			$r1 = mysqli_query($conn, $s1);
			if(mysqli_num_rows($r1)==1) {
				if($row = mysqli_fetch_assoc($r1)) {
					$_SESSION['role']=$row['role'];
					$_SESSION['username']=$row['username'];
					$_SESSION['name']=$row['name'];
					$_SESSION['uid']=$row['id'];
					$_SESSION['logged']=true;
					if($currentFileName=="showpost.php")
						header("Location: ".$_GET['article']."?auth=trueinfo");
					else 
						header("Location: ".$currentFileName."?auth=trueinfo");
				}
			}
		}
		else
			header("Location: ".$currentFileName."?auth=falseinfo");
		}
	}
	else
		header("Location: ".$currentFileName."?auth=falseinfo");
}//end of login modal function


if(isset($_POST['regButton'])) { //registration functionality
	$name = sanitize($_POST['regName']);
	$pwd = sanitize($_POST['regPwd']);
	$usr = sanitize($_POST['regUser']);
	$discr = sanitize($_POST['regDisc']);
	$email = sanitize($_POST['regMail']);
	$usr_pic = sanitize($_FILES["personalthumb"]); //magic happens 
	$currDate = $currDate;

	$temp =$_FILES['personalthumb']['name'];
	if(!$temp) 
		$usr_pic="default.png";
	else 
		$usr_pic=imgUpload($usr_pic);
	
	$sql = "SELECT * FROM login WHERE email='$email' ";
	$result = mysqli_query($conn, $sql);

	if(mysqli_num_rows($result)==0) { //verify that email is available to use.
		$filler = bin2hex(random_bytes(13));
		$salt = '$2a$07$'.$filler;	
		$encryped_pwd = crypt($pwd, $salt);
		$sql = "INSERT INTO login(email, password, reg_date) VALUES ('$email', '$encryped_pwd','$currDate')";
		$result = mysqli_query($conn, $sql);

		if($result) {
			$q = "SELECT id FROM login WHERE email='$email'";
			$r = mysqli_query($conn, $q);
			
			if(mysqli_num_rows($r)>0) {
				if($row = mysqli_fetch_assoc($r)) {
					$id = $row['id'];
					$sql = "INSERT INTO user(id, username, name, description, picture,reg_date) VALUES ('$id', '$usr', '$name', '$discr', '$usr_pic','$currDate')";
					$result = mysqli_query($conn, $sql);

					header("Location: register?reg=successful");
				}
			}
		}
	}
	else if(mysqli_num_rows($result)==1) //verify that email is available to use.
		header("Location: register?reg=dubbed");
	else
		header("Location: register?reg=error");

	if ((move_uploaded_file($_FILES['personalthumb']['tmp_name'], $uploadfile))) 
		echo "naisu.";
	else 
		echo "<br>error";
}

if(isset($_POST['addAComment'])) { //add a comment functionality
//get username from database
	$id=$_SESSION['uid'];
	$sql = "SELECT username FROM user WHERE id='$id' ";
	$result = mysqli_query($conn, $sql);
	$row = mysqli_fetch_assoc($result);
	$by = $row['username'];

	$commentBy = sanitize($by);
	$commentDisc = sanitize($_POST['commentDisc']);
	$art = $_GET['article'];

	$sql = "UPDATE post SET views=views-2 WHERE id={$_GET['article']}";
	$result = mysqli_query($conn, $sql);
	if(!$result)
		echo "Sorry, an error encountered when posting your comment.";

	$sql = "INSERT INTO comment (post_id, username, description,date) VALUES ('$art', '$commentBy', '$commentDisc', '$currDate')";
	$result = mysqli_query($conn, $sql);
	$currDate = $currDate;
	if(!$result)
		echo "Sorry, an error encountered when posting your comment.";
	else 
		header("Location: /feed/showpost.php?article=".$art."#thecomments");
}

if(isset($_POST['editProfile'])) {
	header("Location: userdash?modify=on");
}

if(isset($_POST['saveProfile'])) { //edit profile functionality
//magic starts here.
	$picture = sanitize($_FILES["personalthumb"]);
	$picture = imgUpload($picture);
	//--
	$uName = sanitize($_POST['profUser']);
	$fName = sanitize($_POST['profName']);
	$disc = sanitize($_POST['profDisc']);

	if(!$picture) 
		$sql = "UPDATE user SET username='$uName', name='$fName', description='$disc' WHERE id=".$_SESSION['uid'];
	else 
		$sql = "UPDATE user SET username='$uName', name='$fName', description='$disc', picture='$picture' WHERE id=".$_SESSION['uid'];
	$result = mysqli_query($conn, $sql);
	if($result) 
		header("Location: ".$currentFileName."?update=success".$temp);
	else
		header("Location: ".$currentFileName."?update=fail");
}



?>


