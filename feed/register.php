<?php 
include $_SERVER['DOCUMENT_ROOT']."/feed/includes/header.php";

if(!empty($_SESSION['logged'])==true) 
	header('Location: /feed/articles')
?>

<div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
	<h1 class="display-4 font-italic text-center">Do what you love.</h1>
	<h5 class="font-weight-lighter text-center">Welcome.</h5>
</div>

<div class="container">
<div class="col">	
<?php 
	if(isset($_GET['reg'])=="dubbed") {
?>
	<div class="alert alert-info" role="alert">
		You're already registered. Please use your email to <b><a href="" data-toggle="modal" data-target="#exampleModal">login!</a></b>
	</div>
	<br><hr><br>
<?php
	}
	else if(isset($_GET['reg'])=="error") {
?>		
	<div class="alert alert-warning" role="alert">
		Sorry, we encountered an error. Please try again or contact me at: <b><a href="emazzy@mazzymoo.com"></a></b>
	</div>
	<br><hr><br>
<?php	
	}
	else if(isset($_GET['reg'])=="successful") {
?>		
	<div class="alert alert-success" role="alert">
		<b>Hurray!</b><br> You registered successfully!
	</div>
	<br><hr><br>
<?php
	}
	else
		echo "";
?>
	<h3 class="font-weight-light text-center font-italic">Yay!<br>Come join us =)</h3>
</div>	
<br><hr><br>



<form action="register" method="post" enctype="multipart/form-data">
<div class="col-6 mx-auto">	
	<div class="form-group">
		<label for="regUser">Username</label>
		<input type="text" class="form-control" id="regUser" name="regUser" placeholder="We will call you by this name" required="">
	</div>
  	<div class="form-group">
	    <label for="regMail">Email address</label>
	    <input type="email" class="form-control" id="regMail" name="regMail" aria-describedby="emailHelp" placeholder="This will be your login ID" required="">
	</div>
	<div class="form-group">
    	<label for="regPwd">Password</label>
    	<input type="password" class="form-control" id="regPwd" name="regPwd" placeholder="Your password" pattern=".{6,}" required="">
    </div>
	<div class="form-group">
		<label for="regName">Full Name</label>
		<input type="text" class="form-control" id="regName" name="regName" placeholder="You have a nice name" required="">
	</div>
	<div class="form-group">
		<label for="personalthumb" class="lead">Profile picture</label>
		<input type="file" class="form-control-file" name="personalthumb" id="personalthumb" >
	</div>
	<br>
  	<div class="form-group">
  		<button id="regButton" name="regButton" type="submit" class="btn btn-success">Register now!</button>
 	</div>
</div>
</form>

</div> <!-- end of container -->
<br>






<?php include "../includes/footer.php"; ?>