<?php 
	include $_SERVER['DOCUMENT_ROOT']."/feed/includes/header.php";

	if(!$_SESSION['logged']) 
		header('Location: /feed/articles');


	$sql = "SELECT * FROM user WHERE id= ".$_SESSION['uid'];
	$result = mysqli_query($conn, $sql);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
	$uName = $row['username'];
	$fName = $row['name'];
	$picture = $row['picture'];
	$disc = $row['description'];

	$sql = "SELECT email FROM login WHERE id= ".$_SESSION['uid'];
	$result = mysqli_query($conn, $sql);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
	$email = $row['email'];
?>
<div class="jumbotron p-4p-md-5 text-dark rounded bg-light">
	<h5 class="font-weight-lighter text-center">Welcome back, <strong><?php echo $uName; ?></strong></h5>
</div>
<section>
	<div class="container">

		<form action="userdash" method="post" enctype="multipart/form-data">
<?php if(isset($_GET['update']) && $_GET['update']=="success") { ?>
<div class="alert alert-primary" role="alert">
  Ouh la la~
</div>
<br>
<?php } if(isset($_GET['update']) && $_GET['update']=="fail") { ?>
<div class="alert alert-danger" role="alert">
  Oopsy doopsy. How about you report this error? =)
</div>
<br>
<?php } ?>
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<label for="personalthumb" class="lead">Profile picture</label>
        			<br>
        			<img src="/img/users/<?php echo $allUsers['picture']; ?>" onerror="this.onerror=null;this.src='/img/users/<?php echo $picture; ?>';" class="img-thumbnail" title="" style="border-radius: 8px; width:200px;height:200px;" alt="No Display." >
					<br>
					<br>
					<input type="file" class="form-control-file" name="personalthumb" id="personalthumb" <?php if(empty($_GET['modify']) && isset($_GET['modify'])!="on")echo 'disabled=""';?>>
				</div>
			</div>
			<div class="col-lg-7">
				<div class="form-group">
					<label for="regUser">Username</label>
					<input type="text" class="form-control" id="profUser" name="profUser" value="<?php echo $uName; ?>" required="" <?php if(empty($_GET['modify']) && isset($_GET['modify'])!="on")echo 'disabled=""';?>>
				</div>
			  	<div class="form-group">
				    <label for="regMail">Email </label>
				    <input type="email" class="form-control" id="profMail" name="profMail" aria-describedby="emailHelp" placeholder="<?php echo $email; ?>" required="" disabled="">
				</div>
				<div class="form-group">
					<label for="regName">Full Name</label>
					<input type="text" class="form-control" id="profName" name="profName" value="<?php echo $fName; ?>" required="" <?php if(empty($_GET['modify']) && isset($_GET['modify'])!="on")echo 'disabled=""';?>>
				</div>
				<div class="form-group">
					<label for="regName">About you</label>
					<input type="text" class="form-control" id="profDisc" name="profDisc" placeholder="<?php if($disc!="") echo $disc;  else echo "Tell us more about yourself. Idk, anything can spark a conversation :>"?>" <?php if(empty($_GET['modify']) && isset($_GET['modify'])!="on")echo 'disabled=""';?> value="<?php echo $disc; ?>">
				</div>
			  	<div class="form-group">
			  		<?php if(empty($_GET['modify']) && isset($_GET['modify'])!="on") {?>
			  			<button id="editProfile" name="editProfile" type="submit" class="btn btn-info">Edit Profile</button>
		  			<?php } 
		  				else {
					?>
			  			<button id="saveProfile" name="saveProfile" type="submit" class="btn btn-warning">Save Changes</button>
					<?php } ?>
			 	</div>
			</div>
		</div>
		</form>
		
	</div>
</section>

<?php 

include $_SERVER['DOCUMENT_ROOT']."/includes/footer.php";


?>