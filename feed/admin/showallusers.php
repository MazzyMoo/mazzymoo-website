<?php
  $sql = "SELECT * FROM user ORDER BY reg_date DESC";
  $result = mysqli_query($conn, $sql);
?>

<h5 class="card-title">All Users</h5>
<div class="table-responsive-xl">
<table class="table text-left table-hover">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Picture</th>
      <th scope="col">Username</th>
      <th scope="col">Role</th>
      <th scope="col">Description</th>
      <th scope="col">Reg. Date</th>
    </tr>
  </thead>
  <tbody>
<?php
  while($allUsers = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
?>
    <tr>
      <th scope="row"><?php echo $allUsers['id']; ?></th>
      <td>
        <img src="/img/users/<?php echo $allUsers['picture']; ?>" onerror="this.onerror=null;this.src='/img/thumbnail.svg';" class="img-thumbnail" title="" style="border-radius: 8px; width:100px;height:100px;" alt="No Display." >
      </td>
      <td><?php echo $allUsers['username']; ?></td>
      <td><?php echo $allUsers['role']; ?></td>
      <td><?php echo $allUsers['description']; ?></td>
      <td><?php echo date("F, Y ", strtotime($allUsers['reg_date'])); ?></td>
    </tr>
<?php } ?>
  </tbody>


</table>
</div>