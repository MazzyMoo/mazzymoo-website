<?php 
include $_SERVER['DOCUMENT_ROOT']."/feed/admin/includes/header.php";

$db_host = "localhost";
$db_username = "root";
$db_password = "root";
$db_name = "u581901668_pub"; 

$conn = mysqli_connect($db_host, $db_username, $db_password, $db_name);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

?>

<section class="bg-light">
<div class="container">
	<h3 class=""> Welcome back <b><?php echo $_SESSION['username']; ?> </b> </h3>
	<hr>

<div class="card text-center">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs" id="dashtab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="mail-tab" data-toggle="tab" aria-controls="mailtab" href="#mailtab" >Mail</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#sublist" id="sub-tab" data-toggle="tab" aria-controls="sublist" >Subscribers</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
<div class="tab-content" id="myTabContent">

<div class="tab-pane fade show active" id="mailtab" role="tabpanel" aria-labelledby="articles-tab">
    <?php include $_SERVER['DOCUMENT_ROOT']."/feed/admin/showallmail.php";?>
</div>
<div class="tab-pane fade" id="sublist" role="tabpanel" aria-labelledby="users-tab">   
	<?php include $_SERVER['DOCUMENT_ROOT']."/feed/admin/showallsubs.php";?>
</div>
</div>
  </div>
</div>


</div>
</section>



<?php include $_SERVER['DOCUMENT_ROOT']."/includes/footer.php"; ?>

