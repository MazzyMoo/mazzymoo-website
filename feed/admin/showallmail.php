<?php
	
	$sql = "SELECT * FROM getintouch ORDER BY 'Time' DESC";
  $result = mysqli_query($conn, $sql);

?>


<h5 class="card-title">Mail: Get in touch!</h5>
<div class="table-responsive-xl">
<table class="table text-left table-hover">
  <thead>
    <tr>
      <th scope="col">Subject</th>
      <th scope="col">Comment</th>
      <th scope="col">Name</th>
      <th scope="col">Time</th>
    </tr>
  </thead>
  <tbody>
<?php
  while($allMail = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
?>
    <tr>
      <th scope="row"><?php echo $allMail['Name']; ?></th>
      <td><?php echo $allMail['Comment']; ?></td>
      <td><?php echo $allMail['Subject']; ?></td>
      <td><?php echo $allMail['Time']; ?></td>
    </tr>
<?php } ?>
  </tbody>


</table>
</div>