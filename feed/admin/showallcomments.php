<?php
  $sql = "SELECT * FROM comment ORDER BY date DESC";
  $result = mysqli_query($conn, $sql);
?>

<h5 class="card-title">All Comments</h5>
<div class="table-responsive-xl">
<table class="table text-left table-hover">
  <thead>
    <tr>
      <th scope="col">Post</th>
      <th scope="col">Username</th>
      <th scope="col">Comment</th>
      <th scope="col">Date</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
<?php
  while($allComments = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
?>
    <tr>
      <th scope="row"><?php echo $allComments['post_id']; ?></th>
      <td><?php echo $allComments['username']; ?></td>
      <td><?php echo $allComments['description']; ?></td>
      <td><?php echo date("F, Y", strtotime($allComments['date'])); ?></td>
      <td>
      	<form method="post" action="?delete_comment=<?php echo $allComments['id']; ?>">
          <button type="submit" class="btn btn-danger">DELETE</button>
        </form>
      </td>
    </tr>
<?php } ?>
  </tbody>


</table>
</div>