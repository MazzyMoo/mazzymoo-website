<?php 
include $_SERVER['DOCUMENT_ROOT']."/feed/admin/includes/header.php";
?>

<section class="bg-light">
<div class="container">
	<h3 class=""> Welcome back <b><?php echo $_SESSION['username']; ?> </b> </h3>
	<hr>

<div class="card text-center">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs" id="dashtab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="articles-tab" data-toggle="tab" aria-controls="articlestab" href="#articlelist" >Articles</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#userlist" id="users-tab" data-toggle="tab" aria-controls="userstab" >Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#catlist" id="cat-tab" data-toggle="tab" aria-controls="cattab" >Categories</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#commentlist" id="comment-tab" data-toggle="tab" aria-controls="commenttab" >Comments</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
<div class="tab-content" id="myTabContent">

<div class="tab-pane fade show active" id="articlelist" role="tabpanel" aria-labelledby="articles-tab">
    <?php include $_SERVER['DOCUMENT_ROOT']."/feed/admin/showallarticles.php";?>
</div>
<div class="tab-pane fade" id="userlist" role="tabpanel" aria-labelledby="users-tab">   
	<?php include $_SERVER['DOCUMENT_ROOT']."/feed/admin/showallusers.php";?>
</div>
<div class="tab-pane fade" id="catlist" role="tabpanel" aria-labelledby="users-tab">    
    <?php include $_SERVER['DOCUMENT_ROOT']."/feed/admin/showallcats.php";?>
</div>
<div class="tab-pane fade" id="commentlist" role="tabpanel" aria-labelledby="users-tab">    
    <?php include $_SERVER['DOCUMENT_ROOT']."/feed/admin/showallcomments.php";?>
</div>

</div>
  </div>
</div>

</div>
</section>


















<?php include $_SERVER['DOCUMENT_ROOT']."/includes/footer.php"; ?>
