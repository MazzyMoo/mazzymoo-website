<?php 
include $_SERVER['DOCUMENT_ROOT']."/feed/admin/includes/header.php";

$get_cats = "SELECT * FROM category";
$result_cats = mysqli_query($conn, $get_cats);
?>
<section class="bg-light">
<div class="container">
<h3> Publish an Article. </h3>

<form method="post" action="addarticle.php" enctype="multipart/form-data">



  <div class="form-row">
  <div class="col-md-6">
  	<label for="articletitle" class="lead">Title</label>
  	<input id="articletitle" name="articletitle" type="text" class="form-control" placeholder="" required="">
  </div>
  </div>

  <div class="form-row">
  <div class="col-md-2 ">
    <select class="custom-select form-control form-control-sm" id="catslection" name="catslection" required="">
      <option value="" selected>Category</option>
      <?php 
        while($rows = mysqli_fetch_array($result_cats, MYSQLI_ASSOC)) {
      ?>
      <option value="<?php echo $rows['id']; ?>"><?php echo $rows['name']; ?></option>
      <?php } ?>
    </select>
  </div>
  </div>
  <br>
  <div class="form-row">
  <div class="col-md-11">
  	<label for="articlecontent" class="lead">Content</label>
    <!--project starts here.-->
    <script>
      tinymce.init({
        selector: '#articlecontent'
      });
    </script>

  	<!--textarea id="articlecontent" name="articlecontent" rows="10" required=""></textarea-->
    <textarea name="articlecontent" id="articlecontent"></textarea>

  </div>
  </div>
  <br>

  <div class="form-group">
  	<label for="articlethumb" class="lead">Upload thumbnail</label>
  	<input type="file" class="form-control-file" name="articlethumb" id="articlethumb" required="">
  </div>

  <br>
  <div class="form-row">
  <div class="col-md-4">
    <!--button id="draftarticle" name="draftarticle" type="submit" class="btn btn-outline-success btn-lg">Save as a draft</button-->
  </div>
  <div class="col-md-2 ml-auto">
    <button id="publisharticle" name="publisharticle" type="submit" class="btn btn-outline-info btn-lg" value="publisharticle">Publish</button>
  </div>
  </div>





</form>	
</div>
</section>
	
	





<?php 
include $_SERVER['DOCUMENT_ROOT']."/includes/footer.php";
?>
