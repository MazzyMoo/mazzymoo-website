<?php

session_start();

if(!isset($_SESSION['role']) || $_SESSION['role']!='super'){
  header('Location: /feed/showpost?badattempt');
}

include_once $_SERVER['DOCUMENT_ROOT']."/feed/includes/db.php"; 
include_once $_SERVER['DOCUMENT_ROOT']."/feed/admin/includes/admin_functions.php"; 

$currentFileName = basename($_SERVER['PHP_SELF']);

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="description" content="The Feed: Articles">
  <meta name="keywords" content="Portfolio,Emad,Bamatraf,MazzyMoo,MazMozdy,personal website,thefeed,blog,vlog,a world within brackets,articles,desgin,projects,halifax,canada,novascotia,dalhousie,bacs,computerscience,international,cooking,off-campus,food,tips">
  <meta name="author" content="Emad Bamatraf, MazzyMoo, MazMozdy, Maz, Mazzy, Moo">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Mazzy: Dashboard</title>

  <!-- CSS files -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
  <link href="/styles/style.css" rel="stylesheet">

  <!-- TinyMCE -->
  <script src="https://cdn.tiny.cloud/1/s36siqvgx0w1tsdaeyhgnjxrn1i4olt3xx4mo7p6wihcvybj/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

</head>

<nav class="navbar navbar-expand-sm navbar-light sticky-top" id="mainNav">
<div class="container">
<a class="navbar-brand js-scroll-trigger" href="/index.php"><img src="/img/brandbox.png" width="40" height="40" class="d-inline-block align-top" alt=""> Mazzy</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <a class="small text-warning">Feed</a>
<ul class="navbar-nav ml-auto">
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger <?php if($currentFileName=='articles.php') {echo 'show';} ?>" href="/feed/articles.php">Articles</a>
  </li>
  <?php if($_SESSION['logged']==false) { ?>
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="" data-toggle="modal" data-target="#exampleModal">Login</a>
  </li>
  <?php } ?>

  <?php if($_SESSION['logged']==true) { ?>
  <li class="nav-item">
    <form id="login_modal" method="post" action="<?php echo "/feed/articles.php?logout=true" ?>">
      <button id="logoutbutton" name="logoutbutton" type="submit" class="btn btn-link nav-link" style="font-size: inherit;" >Logout</button>
    </form>
  </li>
  <?php } ?>

</ul>
</div>
</div>
</nav>

<body>
<?php include_once $_SERVER['DOCUMENT_ROOT']."/feed/includes/logform.php";

if($_SESSION['logged'] && $_SESSION['role']=='super') {
?>
<ul class="nav justify-content-end">
  <li class="nav-item">
    <div class="lead small text-right" style="margin-top: 5px; margin-right: 15px;"><a href="/feed/admin/dashboard.php">Dashboard</a></div>
  </li>
  <li class="nav-item">
  <div class="lead small text-right" style="margin-top: 5px; margin-right: 15px;"><a href="/feed/admin/clientsupport.php">Client Support</a></div>
  </li>
</ul>
<?php } 
if(!$_SESSION['logged']){ ?>  
  <p class="lead small text-right" style="margin-top: 5px; margin-right: 15px;">Not part of the crew? You can <a href="register.php">Reigster Now!</a></p>
<?php } 
if($currentFileName=='articles.php' && $_SESSION['logged']){ ?>  
  <div class="alert alert-success text-center" role="alert" >
    Welcome back <b><?php echo $_SESSION['username']?></b>. Enjoy your day ;)
  </div>
<?php
}?>
<?php



