<?php 
date_default_timezone_set('America/Halifax');
function sanitize ($sanitizeThisThing) {
	$sanitizedThing = trim($sanitizeThisThing);
	$sanitizedThing = stripslashes($sanitizedThing);
	$sanitizedThing = htmlspecialchars($sanitizedThing);

	return $sanitizedThing;
}
function sanitizeContent ($sanitizeThisThing) {
	//$sanitizedThing = trim($sanitizeThisThing);
	//$sanitizedThing = stripslashes($sanitizeThisThing);

	return $sanitizeThisThing;
}

if(isset($_GET['delete_post']))
{
	$temp = $_GET['delete_post'];
	$sql = "DELETE FROM post WHERE id={$temp}";
	$result = mysqli_query($conn, $sql);
}

if(isset($_GET['delete_comment']))
{
	$temp = $_GET['delete_comment'];
	$sql = "DELETE FROM comment WHERE id={$temp}";
	$result = mysqli_query($conn, $sql);
}

if(isset($_POST['publisharticle']))
{
	 $filename = $_FILES["articlethumb"]["name"]; 
	 $filetype = $_FILES['articlethumb']['type'];
	 $filename = strtolower($filename);
	 $filetype = strtolower($filetype);	
	//check if file contain php and kill it 
	if(!((strpos($filename,'php')) === false)) {
		die('error; php detected.');
	}
	$file_ext = strrchr($filename, '.');
	//check if the extension is allowed or not
	$whitelist = array(".jpg",".jpeg",".gif",".png"); 
	if (!(in_array($file_ext, $whitelist))) {
		die('<b>'.$file_ext.'</b> Is NOT an allowed extension.<em> Please upload images only!</em>');
	}
	$pos = strpos($filetype,'image');
	//check upload type
	if($pos === false) {
		die('<b>Error: </b>File type is not image.');
	}
	$imageinfo = getimagesize($_FILES['articlethumb']['tmp_name']);
	if($imageinfo['mime'] != 'image/gif' && $imageinfo['mime'] != 'image/jpeg'&& $imageinfo['mime']      != 'image/jpg'&& $imageinfo['mime'] != 'image/png') {
		die('<b>Error: </b> Image size is a no no.'. $imageinfo['mime']);
	}
	//check double file type (image with comment)
	if(substr_count($filetype, '/')>1){
		die('<b>Error: </b>Bad file type.');
	}
	//uploading: 
	$uploaddir = "../../img/articles/";
	if (file_exists($uploaddir)) {} 
	else {  
		mkdir($uploaddir, 0777);  
	}  
	//change the image name
	$thumbnail_name = md5(basename($_FILES['articlethumb']['name']));
	$uploadfile = $uploaddir . $thumbnail_name .$file_ext;

	if ((move_uploaded_file($_FILES['articlethumb']['tmp_name'], $uploadfile))) {
		$finalname = $thumbnail_name.$file_ext;
		$uid = sanitize($_SESSION['uid']);
		$cid = sanitize($_POST['catslection']);
		$ptitle = sanitize($_POST['articletitle']);
		$pcontent = ($_POST['articlecontent']);
		$pthumb = sanitize($finalname);
		$status = 'published';
		$pdate = date("Y-m-d H:i:s");

		$sql = "INSERT INTO post(user_id, category_id, title, content, thumbnail, status, post_date) VALUES ($uid, $cid, '$ptitle', '$pcontent', '$finalname', '$status', '$pdate')";
		$result = mysqli_query($conn, $sql);
		if($result)
			header('Location: /feed/articles.php?success');
		else{
			echo "error: ".mysqli_error($conn);
			//header('Location: /feed/articles.php?failed');
		}
	} 
	else {
		echo "<br>error";
	}
} //end of publish article
?>












