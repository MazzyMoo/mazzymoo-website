<?php
  $sql = "SELECT * FROM post";
  $result = mysqli_query($conn, $sql);

?>

<h5 class="card-title">All Articles</h5>
  <p class="lead small text-left" style="margin-top: 5px; margin-right: 15px;"><a href="/feed/admin/addarticle.php">Post an article</a></p>

<div class="table-responsive-xl">
<table class="table text-left table-hover">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Thumbnail</th>
      <th scope="col">Title</th>
      <th scope="col">Writer</th>
      <th scope="col">Views</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
<?php
  while($allPosts = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $sql_getusername = "SELECT username FROM user WHERE id={$allPosts['user_id']} ORDER BY reg_date DESC";
    $result_getusername = mysqli_query($conn, $sql_getusername);
    $get_username = mysqli_fetch_array($result_getusername, MYSQLI_ASSOC);
?>
    <tr>
      <th scope="row" colspan="1"><?php echo $allPosts['id']; ?></th>
      <td colspan=""><img src="/img/articles/<?php echo $allPosts['thumbnail']; ?>" onerror="this.onerror=null;this.src='/img/thumbnail.svg';" class="img-thumbnail" title="" style="border-radius: 8px; width:100px;height:100px;" alt="No Display." >
      </td>
      <td colspan="1"><a href="<?php echo '/feed/showpost.php?article='.$allPosts['id']; ?>"><?php echo $allPosts['title']; ?></a></td>
      <td colspan="1"><?php echo $get_username['username']; ?></td>
      <td colspan="1"><?php echo $allPosts['views']; ?></td>
      <td colspan="1">
        <form method="post" action="?delete_post=<?php echo $allPosts['id']; ?>">
          <button type="submit" class="btn btn-danger">DELETE</button>
        </form>
      </td>
    </tr>
<?php } ?>
  </tbody>
</table>
</div>