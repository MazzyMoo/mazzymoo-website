<?php
	
	$sql = "SELECT * FROM subscribers ORDER BY number DESC";
  	$result = mysqli_query($conn, $sql);

?>


<h5 class="card-title">All Subscribers</h5>
<div class="table-responsive-xl">
<table class="table text-left table-hover">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Email</th>
      <th scope="col">Subs Date</th>
    </tr>
  </thead>
  <tbody>
<?php
  while($allSubs = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
?>
    <tr>
      <th scope="row"><?php echo $allSubs['number']; ?></th>
      <td><?php echo $allSubs['email']; ?></td>
      <td><?php echo $allSubs['date']; ?></td>
    </tr>
<?php } ?>
  </tbody>


</table>
</div>