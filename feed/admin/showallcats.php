<?php
  $sql = "SELECT * FROM category ORDER BY reg_date DESC";
  $result = mysqli_query($conn, $sql);
?>

<h5 class="card-title">All Categories</h5>
<div class="table-responsive-xl">
<table class="table text-left table-hover">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Posts</th>
      <th scope="col">Opening Date</th>
    </tr>
  </thead>
  <tbody>
<?php
  while($allCats = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
?>
    <tr>
      <th scope="row"><?php echo $allCats['id']; ?></th>
      <td><?php echo $allCats['name']; ?></td>
      <td><?php echo $allCats['total_posts']; ?></td>
      <td><?php echo date("F, Y ", strtotime($allCats['reg_date'])); ?></td>
    </tr>
<?php } ?>
  </tbody>


</table>
</div>