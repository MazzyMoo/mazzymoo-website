<?php 
include $_SERVER['DOCUMENT_ROOT']."/feed/includes/header.php";
?>

<section class="bg-light">
<div class="container">
	<h3> Our Articles. </h3>
<div class="row">
<div class="card-deck" >

<?php
	$sql = "SELECT * FROM post WHERE status='published'";
	$result = mysqli_query($conn, $sql);
  $counter=1;

	while($allPosts = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
		$sql_getusername = "SELECT username FROM user WHERE id={$allPosts['user_id']}";
		$result_getusername = mysqli_query($conn, $sql_getusername);
		$get_username = mysqli_fetch_array($result_getusername, MYSQLI_ASSOC);
?>
<div class="col mx-auto" style="margin-top: 42px">
  <div class="card" style="max-width: 350px; overflow: hidden;" >
    <a href="<?php echo 'article/'.$allPosts['id']; ?>" ><img src="/img/articles/<?php echo $allPosts['thumbnail']; ?>" onerror="this.onerror=null;this.src='/img/thumbnail.svg';" class="card-img-top" alt="Image"></a>
    <div class="card-body" >
      <h5 class="card-title"><a href="<?php echo 'article/'.$allPosts['id']; ?>"><?php echo $allPosts['title']; ?></a></h5>
      <div class="card-text">

      <p>
<?php
      $str = substr($allPosts['content'], 0, 190) . '...</p><p class="text-right">  <a href="article/'.$allPosts['id'].'" class="text-right">Read more...</a></p>';
          echo $str; 
?>    </p>
      </div>
      <?php
        if (strlen($allPosts['content']) > 500)
          $str = substr($allPosts['content'], 0, 7) . '...';
      ?>
      <div class="card-text"><small class="text-muted">By:<b>
      	<?php echo $get_username['username']; ?>
      	</b></small>
      </div>
      <p class="card-text"><small class="text-muted"><?php echo date("F, Y ", strtotime($allPosts['post_date'])); ?></small></p>
    </div>
  </div>
</div>


<?php
  $counter++;
  if($counter==4) {
    $counter=1;
?>
</div> <!-- end of 3rd card deck -->
</div> <!-- end of 3rd col -->
<div class="row">



<?php  } 

} //end of while loop
?>
</div> <!-- end of row -->
</div>  <!-- end of container -->
</section>








<?php 
include $_SERVER['DOCUMENT_ROOT']."/includes/footer.php";
?>
