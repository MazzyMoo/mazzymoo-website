<div class="accordion" id="enablecomments" >
<div class="card">
	<div class="card-header" id="headingOne">
	  <h2 class="mb-0">
	    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
	      <div class="lead">Add your comment</div>
	    </button>
	  </h2>
	</div>

	<div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#enablecomments" id="alibaba" >
  <div class="card-body">
<?php if($_SESSION['logged']==true && $_SESSION['uid']) { //fix session id.
  $id=$_SESSION['uid'];
  $sql = "SELECT username FROM user WHERE id='$id'";
  $result = mysqli_query($conn, $sql);
  $row = mysqli_fetch_assoc($result);
  $commentator = $row['username'];
?>
    <form method="post" action="">
      <div class="form-row">
        <div class="col-md-4">
          <label for="commentBy" class="lead"><strong><?php echo $commentator; ?></strong></label>
          <input type="text" class="form-control" id="commentBy" name="commentBy" aria-describedby="emailHelp" value="<?php echo $commentator; ?>" required="" hidden>
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-6" style="padding-left: 0px;">
        <textarea class="form-control" id="commentDisc" name="commentDisc" rows="3" required="" placeholder="Your comments here."></textarea>
    	</div>
      </div>
      <button id="addAComment" name="addAComment" type="submit" class="btn btn-outline-info btn-lg">Submit</button>
    </form>
<?php } else {?>
    <p>Please <a href="" data-toggle="modal" data-target="#exampleModal"><strong>login</strong></a> order to comment =)</p>
    <hr>
    <p>If you are not a member yet, <a href="/feed/register"><strong>registration</strong></a> takes a couple seconds. No email confirmation is needed =D</p>

<?php } ?>
  </div>
  </div>
</div>
</div>
<br>


