<?php 
include $_SERVER['DOCUMENT_ROOT']."/feed/includes/header.php";


$sql = "SELECT * FROM post WHERE id={$_GET['article']} ORDER BY post_date DESC";
$result = mysqli_query($conn, $sql);
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

$user_id = $row['user_id'];
$writer = 'The Keyboard Hero';
$title = $row['title'];
$content = $row['content'];
$thumbnail = $row['thumbnail'];
$status = $row['status'];
$views = $row['views'];
$date = $row['post_date'];
$sql = "SELECT * FROM user WHERE id={$user_id}";
$result = mysqli_query($conn, $sql);
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
$writer = $row['username'];

$increment_views = "UPDATE post SET views=views+1 WHERE id={$_GET['article']}";
$result_views = mysqli_query($conn, $increment_views);
//echo "<h1>title: ".$title."</h1> "
?>

<section class="bg-light" id="thepost">
<div class="container">
	<div class="card" style="padding: 20px;">
	<div class="card-body">  	
	    <div class="row">
	<div class="col">
	    <h4 class="card-title"><?php echo $title; ?></h4>
	    <h6 class="card-subtitle text-muted mb-2  "><?php echo $writer; ?></h6>
	    <a class="card-link text-muted small"><?php echo date("F j, Y ", strtotime($date)); ?></a>

	</div>
	<div class="col-3-sm offset-3-sm text-right">
		<img src="/img/articles/<?php echo $thumbnail; ?>" onerror="this.onerror=null;this.src='/img/thumbnail.svg';" class="img-thumbnail" title="" style="border-radius: 8px; margin-right: 30px; margin-bottom: 15px; float: left;" alt="No Display." >
	</div>
</div>

	    <hr>
	    <p class="card-text text-justify" style="margin-left: 20px; margin-right: 20px;"><?php echo $content; ?></p>
	    <br><br><hr>

		<div class="row">
			<div class="col">
			    <a class="card-link small">Date: <?php echo date("F j, Y ", strtotime($date)); ?></a>
			</div>
			<div class="col offset text-right">
			    <a class="card-link small">Views: <?php echo $views; ?></a>
			</div>
		</div>
			<a href="../articles" class="card-link">Back</a>
	</div>
	</div>
</div>
</section>


<!-- Comments section --> 
<section id="thecomments">
<div class="container">
<?php include_once "addcomment.php"; ?>
	<div class="card" style=" padding: 20px;">
	<h3> Comments Section: </h3>


	<div class="card-body">
<?php
$sql = "SELECT * FROM comment WHERE post_id={$_GET['article']} ORDER BY date DESC";
$result = mysqli_query($conn, $sql);

if(mysqli_num_rows($result)>0) {
	while($allComments = mysqli_fetch_array($result, MYSQLI_ASSOC)) { 
?> 

		<div class="card border-secondary mb-3 text-center p-3">
			<blockquote class="blockquote mb-0">
			  <p><?php echo $allComments['description']; ?></p>
			  <footer class="blockquote-footer text-right">
			    <small>
			      <cite title="Source Title"><?php echo $allComments['username']; ?></cite>
			    </small>
			  </footer>
			</blockquote>
		</div>


<?php } 
} 
else {
	echo '<h4 class="text-secondary font-italic"><b>Be the first to comment!</b></h4>';
} ?>
	</div>

</div>
</section>





<?php 
include $_SERVER['DOCUMENT_ROOT']."/includes/footer.php";
?>
